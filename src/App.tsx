import React from "react";
import logo from "./logo.svg";
import "./App.css";
import DemoProps from "./DemoProps/DemoProps";
import DemoState from "./DemoState/DemoState";
import DemoForm from "./DemoForm/DemoForm";
import DemoTodoList from "./DemoTodoList/DemoTodoList";

function App() {
  return (
    <div className="App">
      {/* <DemoProps /> */}
      {/* <DemoState /> */}
      {/* <DemoForm /> */}

      <DemoTodoList />
    </div>
  );
}

export default App;
