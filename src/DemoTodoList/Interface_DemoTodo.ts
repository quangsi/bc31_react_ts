export interface InteraceTodo {
  id: string;
  text: string;
  isCompleted: boolean;
}

export interface InterfaceTodoList {
  todos: InteraceTodo[];
  handleTodoRemove: (id: string) => void;
}

export interface InterfaceTodoComponent {
  handleTodoRemove: (id: string) => void;
  todo: InteraceTodo;
}
export interface InterfaceTodoForm {
  handleTodoCreate: (todo: InteraceTodo) => void;
  handleTodoBlur: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
