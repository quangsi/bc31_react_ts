import React from "react";
import { InteraceTodo, InterfaceTodoComponent } from "../Interface_DemoTodo";

type Props = {};

export default function TodoItem({
  todo,
  handleTodoRemove,
}: InterfaceTodoComponent) {
  return (
    <tr className="border-b dark:bg-gray-800 dark:border-gray-700 odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700">
      <th
        scope="row"
        className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap"
      >
        {todo.id}
      </th>
      <td className="px-6 py-4">{todo.text}</td>
      <td className="px-6 py-4 flex items-center space-x-5">
        <input
          className="transform scale-150"
          type="checkbox"
          checked={todo.isCompleted}
        />

        <button
          onClick={() => {
            handleTodoRemove(todo.id);
          }}
          className="p-3 rounded bg-red-600 text-white"
        >
          X
        </button>
      </td>
    </tr>
  );
}
