import { InteraceTodo } from "./Interface_DemoTodo";

export const defaultTodos: InteraceTodo[] = [
  {
    id: "1",
    text: "Làm bài tập",
    isCompleted: false,
  },
  {
    id: "2",
    text: "Làm dự án",
    isCompleted: false,
  },
  {
    id: "3",
    text: "Coi phim",
    isCompleted: false,
  },
];
