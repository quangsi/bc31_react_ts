import React, { useState } from "react";
import { InteraceTodo } from "./Interface_DemoTodo";
import TodoFrom from "./TodoFrom/TodoFrom";
import TodoList from "./TodoList/TodoList";
import { defaultTodos } from "./utiltsDemoTodo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
type Props = {};

export default function DemoTodoList({}: Props) {
  const [todos, setTodos] = useState<InteraceTodo[]>(defaultTodos);

  let handleTodoCreate = (todo: InteraceTodo) => {
    let newTodos = [...todos, todo];

    setTodos(newTodos);
  };
  let handleTodoRemove = (idTodo: string) => {
    const newTodos = todos.filter((todo: InteraceTodo) => {
      return todo.id != idTodo;
    });
    setTodos(newTodos);
    toast("Success");
  };
  //
  let handleTodoBlur = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log("yes", e.target);
    if (e.target.value.length == 0) {
      if (e.target.classList.contains("border-blue-500")) {
        e.target.classList.remove("border-blue-500");
      }
      e.target.classList.add("border-red-500");
    } else {
      if (e.target.classList.contains("border-red-500")) {
        e.target.classList.remove("border-red-500");
      }
      e.target.classList.add("border-blue-500");
    }
  };

  return (
    <div>
      <ToastContainer />
      <TodoFrom
        handleTodoBlur={handleTodoBlur}
        handleTodoCreate={handleTodoCreate}
      />
      <TodoList handleTodoRemove={handleTodoRemove} todos={todos} />
    </div>
  );
}
