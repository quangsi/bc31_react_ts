import React, { useState } from "react";
import { InteraceTodo, InterfaceTodoForm } from "../Interface_DemoTodo";
// @ts-ignore
import shortid from "shortid";
export default function TodoFrom({
  handleTodoCreate,
  handleTodoBlur,
}: InterfaceTodoForm) {
  const [todoTitle, setTodoTitle] = useState<string>("");
  let handleOnChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    setTodoTitle(e.target.value);
  };

  let handleAddTodo = () => {
    let newTodo: InteraceTodo = {
      id: shortid.generate(),
      text: todoTitle,
      isCompleted: false,
    };
    handleTodoCreate(newTodo);
    setTodoTitle("");
  };
  return (
    <div className=" container mx-auto my-20 flex space-x-5">
      <input
        value={todoTitle}
        onBlur={handleTodoBlur}
        onChange={handleOnChangeInput}
        type="text"
        className="p-5 grow border-2 rounded border-gray-600"
        placeholder="Type todo title"
      />
      <button
        onClick={handleAddTodo}
        className="text-white bg-orange-500 rounded px-5 py-2"
      >
        Add todo{" "}
      </button>
    </div>
  );
}
