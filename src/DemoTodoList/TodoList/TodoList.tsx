import React from "react";
import { InterfaceTodoList } from "../Interface_DemoTodo";
import TodoItem from "../TodoItem/TodoItem";

type Props = {};

export default function TodoList({
  todos,
  handleTodoRemove,
}: InterfaceTodoList) {
  return (
    <div className="container mx-auto my-20">
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                Id
              </th>
              <th scope="col" className="px-6 py-3">
                Title
              </th>
              <th scope="col" className="px-6 py-3">
                Is_Completed
              </th>
              {/* <th scope="col" className="px-6 py-3">
                Price
              </th> */}
            </tr>
          </thead>

          <tbody>
            {todos.map((todo) => {
              return (
                <TodoItem handleTodoRemove={handleTodoRemove} todo={todo} />
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
