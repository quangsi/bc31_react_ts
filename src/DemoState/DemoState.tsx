import React, { useState } from "react";

type Props = {};

interface Address {
  street: string;
  number: number;
}
interface InterfaceUser {
  username: string;
  age: number;
  addess: Address;
}

export default function DemoState({}: Props) {
  const [like, setLike] = useState<number>(1);
  const [user, setUser] = useState<InterfaceUser | null>(null);
  return (
    <div className="container mx-auto py-20 text-center text-xl font-medium">
      <span>{like}</span>{" "}
      <button
        onClick={() => {
          setLike(like + 1);
        }}
        className="rounded px-5 py-2 text-white bg-blue-500 "
      >
        Plus like
      </button>
    </div>
  );
}
