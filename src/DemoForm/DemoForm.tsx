import React, { useState } from "react";

type Props = {};

export default function DemoForm({}: Props) {
  const [todoValue, setTodoValue] = useState<string>("");
  let handleOnchangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
  };
  return (
    <div className="container mx-auto my-20 text-center">
      <input
        className="border border-black rounded mr-10 p-5 w-96 "
        placeholder="type todo title"
        onChange={(e) => {
          handleOnchangeInput(e);
        }}
      />
      <button className="rounded text-white px-5 py-2 bg-orange-500">
        Add todo
      </button>
    </div>
  );
}
