export interface InterfaceUser {
  username: string;
  age: number;
  gender: string;
}

export interface InterfaceUserComponent {
  user: InterfaceUser;
  title?: string;
}
