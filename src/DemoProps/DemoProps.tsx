import React from "react";
import { InterfaceUser } from "./InterfaceUser";
import UserInfor from "./UserInfor";

type Props = {};

let userInfor: InterfaceUser = {
  username: "Alice",
  age: 2,
  gender: "Female",
};

export default function DemoProps({}: Props) {
  return (
    <div>
      <UserInfor user={userInfor} />
      <UserInfor title="Have a good day" user={userInfor} />
    </div>
  );
}
