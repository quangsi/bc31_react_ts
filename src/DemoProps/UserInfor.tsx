import React from "react";
import { InterfaceUserComponent } from "./InterfaceUser";

type Props = {};

export default function UserInfor({ user }: InterfaceUserComponent) {
  return (
    <div>
      <p>username : {user.username}</p>
      <p>Age: {user.age}</p>
    </div>
  );
}
